package com.ywang.ticketsys.entity;

import java.util.UUID;

public interface UserPurchaseHistory {
	User getUser();

	UUID getId();

	Ticket getTicket();
}
