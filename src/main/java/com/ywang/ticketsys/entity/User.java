package com.ywang.ticketsys.entity;

import java.util.List;
import java.util.UUID;

public interface User {
	long getId();

	String getFirstName();

	String getLastName();

	Address getAddress();

	UUID getCreditCard();

	Double getBalance();

	void addUserPurchaseHistory(UserPurchaseHistory userPurchaseHistory);

	List<UserPurchaseHistory> getUserPurchaseHistory();
}