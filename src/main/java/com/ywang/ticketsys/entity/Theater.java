package com.ywang.ticketsys.entity;

import java.util.UUID;

public interface Theater {
	UUID getId();

	String getName();

	Address getAddress();

}
