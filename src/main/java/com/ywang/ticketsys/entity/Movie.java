package com.ywang.ticketsys.entity;

import java.util.UUID;

import com.ywang.ticketsys.utils.Genres;
import com.ywang.ticketsys.utils.Ratings;

public interface Movie {
	String getName();

	Genres getGenre();

	Ratings getRating();

	String getSynopsis();
	
	UUID getId();

}
