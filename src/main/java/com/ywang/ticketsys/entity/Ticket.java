package com.ywang.ticketsys.entity;

import java.util.Date;
import java.util.UUID;

public interface Ticket {
	UUID getId();

	Movie getMovie();

	Theater getTheater();

	Date getTime();

	Double getPrice();
}
