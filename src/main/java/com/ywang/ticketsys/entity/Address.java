package com.ywang.ticketsys.entity;

public interface Address {
	String getStreet();

	String getCity();

	String getState();

	String getZip();

	String getCountry();

	User getUser();
}
