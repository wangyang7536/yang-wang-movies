package com.ywang.ticketsys.entity.impl;

import java.util.UUID;

import com.ywang.ticketsys.entity.Movie;
import com.ywang.ticketsys.utils.Genres;
import com.ywang.ticketsys.utils.Ratings;

public class MovieImpl implements Movie {
	private String name;
	private Genres genre;
	private Ratings rating;
	private String synopsis;
	private UUID id;

	@Override
	public String getName() {
		return name;
	}

	@Override
	public UUID getId() {
		return id;
	}

	@Override
	public Genres getGenre() {
		return genre;
	}

	@Override
	public Ratings getRating() {
		return rating;
	}

	@Override
	public String getSynopsis() {
		return synopsis;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setGenre(Genres genre) {
		this.genre = genre;
	}

	public void setRating(Ratings rating) {
		this.rating = rating;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

}
