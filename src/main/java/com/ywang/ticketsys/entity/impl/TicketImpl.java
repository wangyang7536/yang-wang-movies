package com.ywang.ticketsys.entity.impl;

import java.util.Date;
import java.util.UUID;

import com.ywang.ticketsys.entity.Movie;
import com.ywang.ticketsys.entity.Theater;
import com.ywang.ticketsys.entity.Ticket;

public class TicketImpl implements Ticket {
	private UUID id;
	private Movie movie;
	private Theater theater;
	private Date time;
	private Double price;

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public Double getPrice() {
		return price;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public void setTheater(Theater theater) {
		this.theater = theater;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@Override
	public UUID getId() {
		return id;
	}

	@Override
	public Movie getMovie() {
		return movie;
	}

	@Override
	public Theater getTheater() {
		return theater;
	}

	@Override
	public Date getTime() {
		return time;
	}
}
