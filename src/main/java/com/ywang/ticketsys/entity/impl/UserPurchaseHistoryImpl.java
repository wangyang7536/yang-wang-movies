package com.ywang.ticketsys.entity.impl;

import java.util.UUID;

import com.ywang.ticketsys.entity.Ticket;
import com.ywang.ticketsys.entity.User;
import com.ywang.ticketsys.entity.UserPurchaseHistory;

public class UserPurchaseHistoryImpl implements UserPurchaseHistory {
	private User user;
	private UUID id;
	private Ticket ticket;

	public UserPurchaseHistoryImpl(Ticket ticket) {

		this.ticket = ticket;
	}

	@Override
	public User getUser() {
		return user;
	}

	@Override
	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public UUID getId() {
		return id;
	}
}
