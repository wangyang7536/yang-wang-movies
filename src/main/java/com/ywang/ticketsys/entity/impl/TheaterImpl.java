package com.ywang.ticketsys.entity.impl;

import java.util.UUID;

import com.ywang.ticketsys.entity.Address;
import com.ywang.ticketsys.entity.Theater;

public class TheaterImpl implements Theater {
	private UUID id;

	private String name;

	private Address address;

	public void setId(UUID id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public UUID getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Address getAddress() {
		return address;
	}

}
