package com.ywang.ticketsys.entity.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.ywang.ticketsys.entity.Address;
import com.ywang.ticketsys.entity.User;
import com.ywang.ticketsys.entity.UserPurchaseHistory;

public class UserImpl implements User {
	private long id;
	private String firstName;
	private String lastName;
	private Address address;
	private UUID creditCard;
	private List<UserPurchaseHistory> userPurchaseHistoryList;
	private Double balance;

	public void setBalance(Double balance) {
		this.balance = balance;
	}
	@Override
	public Double getBalance() {
		return balance;
	}
	@Override
	public void addUserPurchaseHistory(UserPurchaseHistory userPurchaseHistory) {
		if (this.userPurchaseHistoryList == null) {
			this.userPurchaseHistoryList = new ArrayList<UserPurchaseHistory>();
		}
		userPurchaseHistoryList.add(userPurchaseHistory);
	}
	@Override
	public List<UserPurchaseHistory> getUserPurchaseHistory() {
		return userPurchaseHistoryList;
	}
	@Override
	public long getId() {
		return id;
	}
	@Override
	public String getFirstName() {
		return firstName;
	}
	@Override
	public String getLastName() {
		return lastName;
	}
	@Override
	public Address getAddress() {
		return address;
	}
	@Override
	public UUID getCreditCard() {
		return creditCard;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setCreditCard(UUID creditCard) {
		this.creditCard = creditCard;
	}
}
