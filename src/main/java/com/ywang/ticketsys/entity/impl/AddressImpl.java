package com.ywang.ticketsys.entity.impl;

import java.util.UUID;

import com.ywang.ticketsys.entity.Address;
import com.ywang.ticketsys.entity.User;

public class AddressImpl implements Address {
	private UUID id;
	private String street;
	private String city;
	private String state;
	private String zip;
	private String country;
	private User user;

	public void setStreet(String street) {
		this.street = street;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UUID getId() {
		return id;
	}

	@Override
	public String getStreet() {
		return street;
	}

	@Override
	public String getCity() {
		return city;
	}

	@Override
	public String getState() {
		return state;
	}

	@Override
	public String getZip() {
		return zip;
	}

	@Override
	public String getCountry() {
		return country;
	}

	@Override
	public User getUser() {
		return user;
	}
}
