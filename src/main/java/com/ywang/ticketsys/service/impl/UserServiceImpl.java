package com.ywang.ticketsys.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ywang.ticketsys.entity.Ticket;
import com.ywang.ticketsys.entity.User;
import com.ywang.ticketsys.entity.UserPurchaseHistory;
import com.ywang.ticketsys.entity.impl.UserPurchaseHistoryImpl;
import com.ywang.ticketsys.repository.UserRepository;
import com.ywang.ticketsys.service.UserService;

public class UserServiceImpl implements UserService {
	private UserRepository userRepository;

	@Override
	public void buyTicket(Ticket ticket, User user) {
		if (user.getBalance() > ticket.getPrice()) {
			UserPurchaseHistory userPurchaseHistory = new UserPurchaseHistoryImpl(ticket);
			user.addUserPurchaseHistory(userPurchaseHistory);
		}
	}

	@Override
	public User getUser(long userId) {
		return userRepository.getUser(userId);
	}

	@Override
	public User addUser(User user) {

		long id = userRepository.addUser(user);
		return getUser(id);
	}

	@Override
	public void updateUser(User user) {
		userRepository.update(user);
	}

	@Override
	public List<User> getUsers(String firstName, String lastName) {
		List<User> returnList = new ArrayList<>();

		returnList = userRepository.search(firstName, lastName);

		return returnList;
	}
}
