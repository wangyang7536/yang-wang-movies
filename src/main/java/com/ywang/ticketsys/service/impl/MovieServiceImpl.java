package com.ywang.ticketsys.service.impl;

import java.util.List;
import java.util.UUID;

import com.ywang.ticketsys.entity.Movie;
import com.ywang.ticketsys.repository.MovieRepository;
import com.ywang.ticketsys.service.MovieService;
import com.ywang.ticketsys.utils.Genres;
import com.ywang.ticketsys.utils.Ratings;

public class MovieServiceImpl implements MovieService {
	private MovieRepository movieRepository;

	@Override
	public UUID addMovie(Movie movie) {
		movieRepository.addMovie(movie);
		return movie.getId();
	}

	@Override
	public Movie getMovieByName(String name) {
		return movieRepository.getMovieByName(name);
	}

	@Override
	public Movie getMovieByRate(Ratings rate) {
		return movieRepository.getMovieByRate(rate);
	}

	@Override
	public Movie getMovieByGenre(Genres genre) {
		return movieRepository.getMovieByGenre(genre);
	}

	@Override
	public List<Movie> getAllMovies() {
		return movieRepository.getAllMovies();
	}

	@Override
	public void update(Movie movie) {
		movieRepository.update(movie);
	}
}
