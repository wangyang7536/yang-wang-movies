package com.ywang.ticketsys.service;

import java.util.List;
import java.util.UUID;

import com.ywang.ticketsys.entity.Movie;
import com.ywang.ticketsys.utils.Genres;
import com.ywang.ticketsys.utils.Ratings;

public interface MovieService {
	UUID addMovie(Movie movie);

	Movie getMovieByName(String name);

	Movie getMovieByRate(Ratings rate);

	Movie getMovieByGenre(Genres genre);

	List<Movie> getAllMovies();

	void update(Movie movie);
}
