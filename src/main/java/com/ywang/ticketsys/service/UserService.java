package com.ywang.ticketsys.service;

import java.util.List;

import com.ywang.ticketsys.entity.Ticket;
import com.ywang.ticketsys.entity.User;

public interface UserService {
	User addUser(User user);

	void updateUser(User user);

	User getUser(long userId);
	
	List<User> getUsers(String firstName, String lastName);
	
	void buyTicket(Ticket ticket, User user);
}
