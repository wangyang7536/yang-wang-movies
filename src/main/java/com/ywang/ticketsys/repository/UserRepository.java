package com.ywang.ticketsys.repository;

import java.util.List;

import com.ywang.ticketsys.entity.User;

public interface UserRepository {
	long addUser(User user);

	User getUser(long userId);

	List<User> search(String firstName, String lastName);

	void update(User user);
}
