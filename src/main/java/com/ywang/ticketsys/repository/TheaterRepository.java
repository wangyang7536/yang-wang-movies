package com.ywang.ticketsys.repository;

import java.util.List;
import java.util.UUID;

import com.ywang.ticketsys.entity.Address;
import com.ywang.ticketsys.entity.Theater;

public interface TheaterRepository {
	UUID addTheater(Theater theater);

	Theater getTheaterByName(String name);

	Theater getMovieByAddress(Address address);

	List<Theater> getAllTheaters();

	void update(Theater theater);
}
