package com.ywang.ticketsys.utils;

public enum Genres {
	ACTION, ADVENTURE, COMEDY, DRAMA, CRIME, HORROR, SCIENCEFICTION, WAR, WESTERN, ANIMATION
}
